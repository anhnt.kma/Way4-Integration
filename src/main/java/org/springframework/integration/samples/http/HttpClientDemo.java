/*
 * Copyright 2002-207 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.integration.samples.http;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
/**
 * @author Oleg Zhurakousky
 * @author Gary Russell
 *
 */
public class HttpClientDemo {

	private static Log logger = LogFactory.getLog(HttpClientDemo.class);

	
	@SuppressWarnings("unchecked")
	private static Element toChild(Document doc, String name, Map<String, Object> input) {
		Element elm = doc.createElement(name);
        for(String n:input.keySet()) {
        	Object data = input.get(n);
        	if(data instanceof Map)
        		elm.appendChild(toChild(doc, n, (Map<String, Object>) data));
        	else {
        		Element e = doc.createElement(n);
	        	e.setTextContent(input.get(n).toString());
	        	elm.appendChild(e);
        	}
        }
        return elm;
	}
	
	public static Document transformDomXml(Map<String, Object> input, Templates template) throws ParserConfigurationException, TransformerException {
		DocumentBuilder db = null;
        db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = db.newDocument();
        Document outDoc = db.newDocument();
        
        // PayLater file
        Element rootElm = toChild(doc,"pl-data",input);
        doc.appendChild(rootElm);
        
	    Transformer transformer = template.newTransformer();

	    DOMSource source = new DOMSource(doc);
	    DOMResult result = new DOMResult(outDoc);
	    transformer.transform(source, result);
	    return outDoc;
	}
	
	static String xmlToString(Document doc) throws TransformerException {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(doc), new StreamResult(writer));
		return writer.getBuffer().toString().replaceAll("\n|\r", "");
	}
	
	public static void main(String[] args) {
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
				"/META-INF/spring/integration/http-outbound-config.xml");
		try {
			Path tmpl = Paths.get("C:\\Users\\anhkm\\Desktop\\BNPL\\PLtoWay4UFX.xsl");
			TransformerFactory tFactory =
		    TransformerFactory.newInstance();
		    Transformer transformer = tFactory.newTransformer();
		    Templates template = null;
		    try(InputStream inTmpl = new FileInputStream(tmpl.toFile())) {
			    StreamSource source = new StreamSource(inTmpl);
			    template = tFactory.newTemplates(source);
		    }
		    
		    Map<String,Object> root = new HashMap<>();
		    root.put("cust_gender","FEMALE");
		    root.put("contact_email","anhnt.kma@gmail");
		    Map<String,Object> map = new HashMap<>();
		    root.put("address", map);
		    
			map.put("address_line1","Some where");
			map.put("address_line2","in Nghe An");
			
			RequestGateway requestGateway = context.getBean("requestGateway", RequestGateway.class);
			String reply = requestGateway.echo(xmlToString( transformDomXml( root, template)));
		
			
			//String reply = requestGateway.echo("Hello");
			logger.info("\n\n++++++++++++ Replied with: " + reply + " ++++++++++++\n");
			context.close();
		
//			Document document = generateDomXml(null);
//			String xml = new XMLDocument(document).toString();
//			System.out.println(xml);
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
		}
		
	}

}
