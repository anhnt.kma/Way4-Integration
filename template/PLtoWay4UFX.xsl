<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0"
encoding="UTF-8" indent="yes"/>
<xsl:template match="/">
<UFXMsg direction="Rq" msg_type="Application" version="2.0">
   <MsgId>20210427000001</MsgId>
   <Source app="Web01" />
   <MsgData>
      <Application>
         <RegNumber>20210427000002</RegNumber>
         <OrderDprt>0001</OrderDprt>
         <ObjectType>Client</ObjectType>
         <ActionType>Add</ActionType>
         <Data>
            <Client>
               <ClientType>PR</ClientType>
               <ClientInfo>
                  <ClientNumber>CIF2021042700000000011</ClientNumber>
                  <RegNumberType>CHUNG MINH NHAN DAN</RegNumberType>
                  <RegNumber>20210427</RegNumber>
                  <RegNumberDetails>CA Tinh Dak Nong;VIET NAM</RegNumberDetails>
                  <ShortName>VENKATESH</ShortName>
				  <Citizenship>VNM</Citizenship>
                  <Language>VNM</Language>
                  <MaritalStatus>X</MaritalStatus>
                  <Position>Officer</Position>
                  <BirthDate>1988-03-28</BirthDate>
                  <Gender>
				  <xsl:choose>
					<xsl:when test="/pl-data/cust_gender = 'MALE'">Male</xsl:when>
					<xsl:when test="/pl-data/cust_gender = 'FEMALE'">Female</xsl:when>
				  </xsl:choose>
				   </Gender>
               </ClientInfo>
               <PhoneList>
                  <Phone>
                     <PhoneType>Mobile</PhoneType>
                     <PhoneNumber>0921214488</PhoneNumber>
                  </Phone>
               </PhoneList>
               <BaseAddress>
                  <EMail><xsl:value-of select="/pl-data/contact_email"/></EMail>
                  <AddressLine1><xsl:value-of select="/pl-data/address/address_line1"/></AddressLine1>
                  <AddressLine3><xsl:value-of select="/pl-data/address/address_line2"/></AddressLine3>
               </BaseAddress>
               <AddInfo>
					<ADDDATE01>2000-03-28</ADDDATE01>
               </AddInfo>
            </Client>
         </Data>
         <SubApplList>
            <Application>
               <RegNumber>20210427000004</RegNumber>
               <ObjectType>Contract</ObjectType>
               <ActionType>Add</ActionType>
               <Data>
                  <Contract>
                     <Product>
                        <ProductCode1>PISBNPL</ProductCode1>
                     </Product>
                     <CreditLimit>
                        <FinanceLimit>
                           <Amount>2000000</Amount>
                           <Currency>VND</Currency>
                        </FinanceLimit>
                        <ReasonDetails>set credit limit for liability</ReasonDetails>
						<ActivityPeriod>
							<Activation>Activate</Activation>
							<DateFrom>2021-05-01</DateFrom>
							<DateTo>2025-05-01</DateTo>
						</ActivityPeriod>
                     </CreditLimit>
                  </Contract>
               </Data>
               <SubApplList>
                  <Application>
                     <RegNumber>20210427000007</RegNumber>
                     <ObjectType>Contract</ObjectType>
                     <ActionType>Add</ActionType>
                     <Data>
                        <Contract>
                           <Product>
                              <ProductCode1>00107</ProductCode1>
                           </Product>
                           <CreditLimit>
								<FinanceLimit>
									<Amount>2000000</Amount>
									<Currency>VND</Currency>
								</FinanceLimit>
								<ReasonDetails>set credit limit</ReasonDetails>
								<ActivityPeriod>
									<Activation>Activate</Activation>
									<DateFrom>2021-05-01</DateFrom>
									<DateTo>2025-05-0;</DateTo>
								</ActivityPeriod>
                           </CreditLimit>
						   <AddContractInfo>
								<ADDINFO04>Esigning Number</ADDINFO04>
						   </AddContractInfo>
                        </Contract>
                     </Data>
                     <SubApplList>
                        <Application>
                           <RegNumber>20210427000008</RegNumber>
                           <ObjectType>ContractParameter</ObjectType>
                           <ActionType>AddOrUpdate</ActionType>
                           <Data>
                              <CustomerParameters>
                                 <Parameter>
                                    <Code>BILLING_DAY</Code>
                                    <Value>1</Value>
                                 </Parameter>
                              </CustomerParameters>
                           </Data>
                        </Application>
						<Application>
							<RegNumber>20210427000009</RegNumber>
							<ObjectType>ContractClassifier</ObjectType>
							<ActionType>AddOrUpdate</ActionType>
							<Data>
								<CustomerClassifiers>
									<Classifier>
										<Code>NOTI_SEND</Code>
										<Value>SMP</Value>
									</Classifier>
								</CustomerClassifiers>
							</Data>
						</Application>
						<Application>
							<RegNumber>20210427000010</RegNumber>
							<ObjectType>ContractClassifier</ObjectType>
							<ActionType>AddOrUpdate</ActionType>
							<Data>
								<CustomerClassifiers>
									<Classifier>
										<Code>BNPL_SCHEME</Code>
										<Value>SDGA01</Value>
									</Classifier>
								</CustomerClassifiers>
							</Data>
						</Application>
                     </SubApplList>
                  </Application>
               </SubApplList>
            </Application>
         </SubApplList>
      </Application>
   </MsgData>
</UFXMsg>
</xsl:template>

</xsl:stylesheet>